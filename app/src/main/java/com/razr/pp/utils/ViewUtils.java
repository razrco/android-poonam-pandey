package com.razr.pp.utils;

import android.view.View;
import android.widget.TextView;

public class ViewUtils {
    public static void setText(TextView textView, CharSequence text) {
        setText(textView, textView, text, true);
    }

    public static void setText(TextView textView, CharSequence text, boolean removeView) {
        setText(textView, textView, text, removeView);
    }

    public static void setText(TextView textView, View containerView, CharSequence text, boolean removeView) {
        if (android.text.TextUtils.isEmpty(text)) {
            containerView.setVisibility(removeView ? View.GONE : View.INVISIBLE);
        } else {
            textView.setText(text);
        }
    }
}
