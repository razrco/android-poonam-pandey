package com.razr.pp.utils;

import android.util.Log;

import com.razr.pp.BuildConfig;


public class LogUtils {
    private static final String LOG_PREFIX = "CD_";

    public static String makeLogTag(String str)
    {
        return LOG_PREFIX + str;
    }

    /**
     * Don't use this when obfuscating class names!
     */
    @SuppressWarnings("rawtypes")
    public static String makeLogTag(Class cls)
    {
        return makeLogTag(cls.getSimpleName());
    }

    public static void d(final String tag, String message) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, message);
        }
    }

    public static void d(final String tag, String message, Throwable cause) {
		if (BuildConfig.DEBUG) {
            Log.d(tag, message, cause);
        }
    }

    public static void v(final String tag, String message) {
		if (BuildConfig.DEBUG) {
            Log.v(tag, message);
        }
    }

    public static void v(final String tag, String message, Throwable cause) {
		if (BuildConfig.DEBUG) {
            Log.v(tag, message, cause);
        }
    }

    public static void i(final String tag, String message) {
		if (BuildConfig.DEBUG) {
            Log.i(tag, message);
        }
    }

    public static void i(final String tag, String message, Throwable cause) {
		if (BuildConfig.DEBUG) {
            Log.i(tag, message, cause);
        }
    }

    public static void w(final String tag, String message) {
		if (BuildConfig.DEBUG) {
            Log.w(tag, message);
        }
    }

    public static void w(final String tag, String message, Throwable cause) {
		if (BuildConfig.DEBUG) {
            Log.w(tag, message, cause);
        }
    }

    public static void e(final String tag, String message) {
		if (BuildConfig.DEBUG) {
            Log.e(tag, message);
        }
    }

    public synchronized static void e(final String tag, String message, Throwable cause) {
		if (BuildConfig.DEBUG) {
            Log.e(tag, message, cause);
        }
    }

	public static void logError(Throwable t) {
		if(t != null) {
			if(BuildConfig.DEBUG) {
				t.printStackTrace();
			}
		}
	}

    private LogUtils() {
    }
}
