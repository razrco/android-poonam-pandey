package com.razr.pp.utils;

import android.support.annotation.DrawableRes;
import android.view.View;

import com.razr.pp.R;
import com.razr.pp.typedefs.RequestStatusWrapper;
import com.razr.pp.widgets.EmptyView;

public class NewEmptyViewManager {

	public static void displayLoadingMessage(EmptyView emptyView) {
		if (emptyView != null) {
			emptyView.displayLoadingMessage();
		}
	}

	public static void hideEmptyView(EmptyView emptyView) {
		if (emptyView != null) {
			emptyView.setVisibility(View.GONE);
		}
	}

	public static void updateEmptyView(EmptyView emptyView, @RequestStatusWrapper.Code int errorCode, View.OnClickListener leftButtonListener, View.OnClickListener rightButtonListener) {
		if (emptyView == null) {
			return;
		}

		if (errorCode == RequestStatusWrapper.CODE_LOADING) {
			emptyView.displayLoadingMessage();
			return;
		}

		setDataUsingErrorCode(emptyView, errorCode, null, null);
	}

	private static void setDataUsingErrorCode(EmptyView emptyView, int errorCode, CharSequence retryMessage, View.OnClickListener callback) {
		switch (errorCode) {
			case RequestStatusWrapper.CODE_NO_INTERNET_CONNECTION:
//				emptyView.setData(
//						R.drawable.ic_error_network_failure_inset, retryMessage,
//						"No Internet!",
//						"Uh oh! Looks like you’re not connected to the internet, please check your network settings and try again",
//						callback);
				return;

			case RequestStatusWrapper.CODE_SOCKET_TIMEOUT:
//				emptyView.setData(
//						R.drawable.ic_error_network_failure_inset, retryMessage,
//						"Bad Internet!",
//						"Uh oh! Looks like there's a problem with the internet connection. Please check your connection and try again",
//						callback);
				return;

			case RequestStatusWrapper.CODE_NO_DATA:
//				emptyView.setData(R.drawable.ic_error_generic_inset, retryMessage,
//						"No Data Found!",
//						"Sorry, we couldn't find any data now. Please try again in some time.",
//						callback);
//				return;

			case RequestStatusWrapper.CODE_OFFER_EXPIRED:
//				emptyView.setData(R.drawable.ic_error_generic_inset, retryMessage,
//						"Offer Expired",
//						"Sorry, the offer you are looking for has expired.",
//						callback);
//				return;

			default:
//				emptyView.setData(
//						R.drawable.ic_error_generic_inset, retryMessage,
//						"Something Went Wrong!",
//						"Sorry, we did something bad and have made a note to fix it. Please try again in some time.",
//						callback);
//				return;
		}
	}


	public static void updateEmptyView(EmptyView emptyView, int errorCode, String errorMessage, CharSequence retryMessage, View.OnClickListener callback) {
		if (emptyView == null) {
			return;
		}

		setDataUsingErrorCode(emptyView, errorCode, retryMessage, callback);
	}

	public static void updateEmptyView(EmptyView emptyView, @DrawableRes int iconResourceId,
									   CharSequence title, CharSequence description, CharSequence retryMessage, View.OnClickListener callback) {
		if (emptyView == null) {
			return;
		}

		emptyView.setData(iconResourceId, retryMessage, title, description, callback);
	}
}
