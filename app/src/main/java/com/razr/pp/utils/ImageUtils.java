package com.razr.pp.utils;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class ImageUtils {
    public static void loadImage(ImageView imageView, String url) {
        if (imageView != null && !TextUtils.isEmpty(url))
            Picasso.with(imageView.getContext())
                    .load(url)
//                    .placeholder(R.drawable.placeholder)
                    .fit()
                    .centerCrop()
                    .into(imageView);
    }

    public static void loadImageWithoutCenterCrop(ImageView imageView, String url) {
        if (imageView != null && !TextUtils.isEmpty(url))
            Picasso.with(imageView.getContext())
                    .load(url)
//                    .placeholder(R.drawable.placeholder)
                    .fit()
                    .into(imageView);
    }

    public static void setBackground(View view, int resource, Context context){
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                view.setBackground(ContextCompat.getDrawable(context, resource));
            } else {
                view.setBackgroundDrawable(ContextCompat.getDrawable(context, resource));
            }
        } catch (Throwable t) {
            LogUtils.logError(t);
        }
    }

    public static void setImageResource(ImageView view, int resource, Context context){
        try {
            view.setImageResource(resource);
        } catch (Throwable t) {
            LogUtils.logError(t);
        }
    }
}
