package com.razr.pp.retrofit.interceptors;

import android.text.TextUtils;

import com.razr.pp.typedefs.RequestStatusWrapper;
import com.razr.pp.utils.LogUtils;
import com.razr.pp.utils.Utils;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Add headers to the all the public api calls.
 * Set error codes in case API call fails.
 */
public class HeaderInterceptor implements Interceptor {

	public HeaderInterceptor getInstance() {
		return this;
	}

	@Override
	public Response intercept(Chain chain) throws IOException {

		String query = chain.request().url().query();
		query = TextUtils.isEmpty(query) ? "" : query;

		Request request = chain.request().newBuilder()
				.addHeader("apiKey", Utils.API_KEY)
				.addHeader("celebId", Utils.CELEB_ID)
				.build();

		try {
			Response response = chain.proceed(request);

			if(response.isSuccessful()) {
				int err = RequestStatusWrapper.CODE_SUCCESS;
				try {
					String errorCode = response.headers().get("X-API-ErrorCode");
					err = (errorCode == null) ? RequestStatusWrapper.CODE_SUCCESS : Integer.parseInt(errorCode);
				}
				catch(NumberFormatException e) {
					err = RequestStatusWrapper.CODE_INVALID_API_ERROR_CODE;
					LogUtils.logError(e);
				}

				String msg = response.headers().get("X-API-ErrorMessage");
				if(err != RequestStatusWrapper.CODE_SUCCESS) {
					return response.newBuilder()
							.code(err)
							.body(response.body())
							.message((msg == null) ? "failure" : msg)
							.build();
				}
			}
			return response;
		}
		catch (UnknownHostException e){
			return getNoInternetResponse(request, RequestStatusWrapper.CODE_NO_INTERNET_CONNECTION);
		}
		catch(SocketTimeoutException e) {
			return getNoInternetResponse(request, RequestStatusWrapper.CODE_SOCKET_TIMEOUT);
		}
		catch(Exception e) {
			return getNoInternetResponse(request, RequestStatusWrapper.CODE_SERVER_ERROR);
		}
	}

	private Response getNoInternetResponse(Request request, int errorCode) {
		return new Response.Builder()
				.request(request)
				.protocol(Protocol.HTTP_1_0)
				.code(errorCode)
				.message("No Internet!!")
				.body(ResponseBody.create(MediaType.parse("application/json"), ""))
				.build();
	}
}
