package com.razr.pp.retrofit;

import com.razr.pp.models.Album;
import com.razr.pp.models.CategoryPhotos;
import com.razr.pp.models.Config;
import com.razr.pp.models.Event;
import com.razr.pp.models.Feed;
import com.razr.pp.models.Location;
import com.razr.pp.models.Video;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface API {

    @POST("api/v1/get-config")
    Call<Config> getConfig();

    @POST("api/v1/get-album-photos/{albumId}")
    Call<Album> getAlbumPhotos();

    @POST("api/v1/get-albums?_format=json")
    Call<ArrayList<Album>> getAlbums(@Body HashMap userGroup);

    @POST("api/v1/get-category-photos/{categoryId}?_format=json")
    Call<ArrayList<CategoryPhotos>> getCategoryPhotos(@Body HashMap userGroup, @Path("categoryId") String categoryId);

    @POST("api/v1/get-photos/{albumId}")
    Call<Album> getPhotos(@Path("albumId") String albumId);

    @POST("api/v1/get-feeds?_format=json")
    Call<ArrayList<Feed>> getFeeds(@Body HashMap userGroup);

    @POST("api/v1/get-feed/{feedId}")
    Call<Feed> getFeed(@Path("feedid") String feedId);

    @POST("api/v1/get-events")
    Call<ArrayList<Event>> getEvents();

    @POST("api/v1/get-event-photos/{eventId}")
    Call<Event> getEventPhotos(@Path("eventId") String eventId);

    @POST("api/v1/get-locations")
    Call<ArrayList<Location>> getLocations();

    @POST("api/v1/get-videos?_format=json")
    Call<ArrayList<Video>> getVideos(@Body HashMap userGroup);

    @POST("api/v1/get-video/{videoId}")
    Call<ArrayList<Video>> getVideo(@Path("videoId") String videoId);

}
