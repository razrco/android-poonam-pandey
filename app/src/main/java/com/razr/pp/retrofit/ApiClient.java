package com.razr.pp.retrofit;

import com.razr.pp.BuildConfig;
import com.razr.pp.PP;
import com.razr.pp.retrofit.interceptors.HeaderInterceptor;
import com.razr.pp.utils.MiscUtils;
import com.razr.pp.utils.Utils;
import com.readystatesoftware.chuck.ChuckInterceptor;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    public static final int CONNECTION_TIMEOUT = 15;
    public static final int SOCKET_TIMEOUT = 25;
    public static HttpLoggingInterceptor logging;

    public static String ROOT = "http://gateway.razrmedia.com/";
    public static String IMAGE_BASE_URL = "https://s3-ap-southeast-1.amazonaws.com/armsimages/";
    public static String VideoGyour_URL ="http://streamer5.vidgyor.com/hls/razrcorp/smil:";

    public static String debugUrl;
    public static boolean shouldDebug;
    private static API REST_CLIENT;

    static {
        setupRestClient();
    }

    private ApiClient() {
    }

    public static API get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        httpClient.readTimeout(SOCKET_TIMEOUT, TimeUnit.SECONDS);

        long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
        File httpCacheDir = Utils.getExternalCacheDir(PP.getPPAppContext());
        // TODO Implement a fallback mechanism if external cache directory is not available
        Cache cache = null;
        try {
            if (httpCacheDir != null) {
                cache = new Cache(httpCacheDir, httpCacheSize);
            }
        } catch (Throwable t) {
            cache = null;
        }
        if (cache != null) {
            httpClient.cache(cache);
        }

        /** Interceptors (Order is important) **/
        // Request Header interceptor - Adds the request headers and tests for error codes on response
        httpClient.interceptors().add(new HeaderInterceptor());

        // For debugging
        // Add debug interceptors only if its a debug build
        if (BuildConfig.DEBUG) {

            httpClient.addInterceptor(new ChuckInterceptor(PP.getPPAppContext()));

            // Logging Interceptor (Add this as the last interceptor)
            logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.interceptors().add(logging);
        }

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(getRootUrl())
                .addConverterFactory(GsonConverterFactory.create(MiscUtils.getGsonInstance()))
                .client(httpClient.build());

        Retrofit restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(API.class);
    }

    private static String getRootUrl() {
        if (shouldDebug) {
            return (debugUrl != null && !debugUrl.isEmpty()) ? debugUrl : ROOT;
        } else {
            return ROOT;
        }
    }

    public static void setRootUrl(String url) {
        ApiClient.ROOT = (url.startsWith("http") ? "" : "http://") +
                url +
                (url.endsWith("/") ? "" : "/");
        ApiClient.setupRestClient();
    }

    public static void setDebugMode(boolean shouldDebug) {
        ApiClient.shouldDebug = shouldDebug;
    }

    public static final class ERROR_CODES {
        public static final int ERROR_CODE_UNKNOWN_HOST = 1;
        public static final int ERROR_CODE_JSON_EXCEPTION = 2;
        public static final int ERROR_CODE_SOCKET_TIMEOUT = 5;
        public static final int ERROR_CODE_UNKNOWN = 99;
    }
}
