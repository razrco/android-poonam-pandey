package com.razr.pp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.razr.pp.R;
import com.razr.pp.adapters.SimpleItemDecorator;
import com.razr.pp.adapters.VideoAdapter;
import com.razr.pp.fragments.base.BaseFragment;
import com.razr.pp.models.Album;
import com.razr.pp.models.Video;
import com.razr.pp.retrofit.ApiClient;
import com.razr.pp.utils.ImageUtils;
import com.razr.pp.utils.LogUtils;
import com.razr.pp.utils.Utils;
import com.razr.pp.widgets.ScrollFeedbackRecyclerView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoFragment extends BaseFragment implements ScrollFeedbackRecyclerView.Callbacks{

    @BindView(R.id.ivHeaderSocialMedia)
    ImageView ivHeaderSocialMedia;

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.tvHeaderTitle2)
    TextView tvHeaderTitle2;

    @BindView(R.id.divider_header)
    View divider_header;

    @BindView(R.id.recyclerView)
    ScrollFeedbackRecyclerView recyclerView;

    @BindView(R.id.appBar)
    AppBarLayout appBar;

    VideoAdapter videoAdapter;
    ArrayList<Video> videoList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoAdapter = new VideoAdapter();
        refreshData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_social_media, container,false);
        ButterKnife.bind(this, view);

        ImageUtils.setImageResource(ivHeaderSocialMedia, R.drawable.iv_header_video, getActivity());
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        ivHeaderSocialMedia.getLayoutParams().width = width;
        ivHeaderSocialMedia.getLayoutParams().height = height;

        tvHeaderTitle.setText("EYE CANDY");
        tvHeaderTitle2.setText("VIDEOS");
        divider_header.getLayoutParams().width = tvHeaderTitle2.getWidth();

        recyclerView.addItemDecoration(new SimpleItemDecorator(ContextCompat.getDrawable(getContext(), R.drawable.divider_transparent_8dp), SimpleItemDecorator.VERTICAL_LIST));
        LinearLayoutManager linear = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linear);
        recyclerView.setAdapter(videoAdapter);
        recyclerView.attachCallbacks(this);

        Log.d("adapter count", String.valueOf(recyclerView.getAdapter().getItemCount()));
        return view;
    }

    private void refreshData() {
        HashMap<String, String> body = new HashMap<>();
        body.put("userGroup", Utils.USERGROUP);

        ApiClient.get().getVideos(body).enqueue(new Callback<ArrayList<Video>>() {
            @Override
            public void onResponse(Call<ArrayList<Video>> call, Response<ArrayList<Video>> response) {
                LogUtils.d("video response", "success");
                if(response.body() != null){
                    videoList = response.body();
                    LogUtils.d("response", "success");
                    videoAdapter.addItems(videoList);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Video>> call, Throwable t) {
                LogUtils.d("response", "failure");
            }
        });
    }

    @Override
    public boolean isAppBarCollapsed() {
        return false;
    }

    @Override
    public void setExpanded(boolean expanded) {
    }
}
