package com.razr.pp.adapters;

import com.razr.pp.R;
import com.razr.pp.models.Feed;
import com.razr.pp.widgets.SocialMediaWidget;

public class SocialMediaAdapter extends AbsPaginatedEvTolerantRecyclerViewAdapter<Feed, SocialMediaWidget>{

    public SocialMediaAdapter(){
        super(R.layout.row_social_media, false);
    }

    @Override
    public void onItemClicked(Feed item) {
        super.onItemClicked(item);
    }

}
