package com.razr.pp.adapters;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.razr.pp.R;
import com.razr.pp.typedefs.RequestStatusWrapper;
import com.razr.pp.utils.NewEmptyViewManager;
import com.razr.pp.widgets.EmptyView;
import com.razr.pp.widgets.rowitems.ListItemInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Abstract EmptyView tolerant RecyclerView Adapter
 * Why yes! That is a super long name
 *
 * @param <DataItemClass> class of the items that are to be stored in the adapter
 */
public class AbsPaginatedEvTolerantRecyclerViewAdapter<DataItemClass, ViewClass extends View>
		extends RecyclerView.Adapter<SimpleRVHolder> implements View.OnClickListener, View.OnLongClickListener {

	private final static int TYPE_NORMAL = 0, TYPE_LOADING = 1;
	public final static int PAGE_THRESHOLD = 0, FIRST_PAGE = 1, ITEM_PER_PAGE = 20;

	public View.OnClickListener retryListener;

	@RequestStatusWrapper.Code
	public int requestStatusCode = RequestStatusWrapper.CODE_NULL;
	public ArrayList<DataItemClass> entries;

	private int rowLayoutId = -1;
	private boolean hasPagination = false;

	private TextView tvLoadMoreMessage;
	public View emptyView;

	public AbsPaginatedEvTolerantRecyclerViewAdapter(int rowLayoutId) {
		this(rowLayoutId, true);
	}

	public AbsPaginatedEvTolerantRecyclerViewAdapter(int rowLayoutId, boolean hasPagination) {
		this.rowLayoutId = rowLayoutId;
		this.hasPagination = hasPagination;
		this.entries = new ArrayList<DataItemClass>();

		registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
			@Override
			public void onChanged() {
				super.onChanged();
				updateEmptyView();
			}

			@Override
			public void onItemRangeChanged(int positionStart, int itemCount) {
				super.onItemRangeChanged(positionStart, itemCount);
			}

			@Override
			public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
				super.onItemRangeChanged(positionStart, itemCount, payload);
			}

			@Override
			public void onItemRangeInserted(int positionStart, int itemCount) {
				super.onItemRangeInserted(positionStart, itemCount);
			}

			@Override
			public void onItemRangeRemoved(int positionStart, int itemCount) {
				super.onItemRangeRemoved(positionStart, itemCount);
			}

			@Override
			public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
				super.onItemRangeMoved(fromPosition, toPosition, itemCount);
			}
		});
	}

	public void setEmptyView(View emptyView) {
		this.emptyView = emptyView;
		updateEmptyView();
	}

	public void setRetryListener(View.OnClickListener retryListener){
		this.retryListener = retryListener;
	}

	public void setRequestStatusCode(@RequestStatusWrapper.Code int code) {
		this.requestStatusCode = code;
		if(getItemCount() == 0) {
			notifyDataSetChanged();
		}
	}

	@RequestStatusWrapper.Code
	public int getRequestStatusCode() {
		return requestStatusCode;
	}

	/**
	 * Helper function to update the emptyView. Displays the emptyView only if the itemCount == 0, else hides it.
	 */
	public void updateEmptyView() {
		if(emptyView != null) {
			emptyView.setVisibility(getItemCount() == 0 ? View.VISIBLE : View.GONE);
		}

		if(requestStatusCode == RequestStatusWrapper.CODE_NULL) {
			return;
		}

		if(emptyView != null && emptyView instanceof EmptyView) {
			switch(requestStatusCode) {
				case RequestStatusWrapper.CODE_NULL:
					return;

				case RequestStatusWrapper.CODE_LOADING:
					NewEmptyViewManager.displayLoadingMessage((EmptyView) emptyView);
					break;

				case RequestStatusWrapper.CODE_NO_INTERNET_CONNECTION:
				case RequestStatusWrapper.CODE_SOCKET_TIMEOUT:
					 NewEmptyViewManager.updateEmptyView((EmptyView) emptyView, requestStatusCode, null, "RETRY", retryListener);
					break;

				case RequestStatusWrapper.CODE_SUCCESS:
				case RequestStatusWrapper.CODE_NO_DATA:
					if(getItemCount() == 0) {
						NewEmptyViewManager.updateEmptyView((EmptyView) emptyView, -1, null,
								"Sorry, we couldn't find any data here", null,
								null);
					}
					break;

				default:
					NewEmptyViewManager.updateEmptyView((EmptyView) emptyView, requestStatusCode, new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							onEmptyViewPrimaryButtonPressed();
						}
					}, new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							onEmptyViewSecondaryButtonPressed();
						}
					});
					break;
			}
		}
	}

	public void clear() {
		entries.clear();
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				notifyDataSetChanged();
			}
		});
	}

	public void setList(DataItemClass... objects) {
		this.entries.clear();
		addItems(objects);
	}

	public void setList(List<DataItemClass> objects) {
		this.entries.clear();
		addItems(objects);
	}

	public void addItems(DataItemClass... objects) {
		if(objects != null) {
			this.addItems(Arrays.asList(objects));
		}
	}

	public void addItems(List<DataItemClass> objects) {
		if(objects != null) {
			for(DataItemClass item : objects) {
				if(item != null) {
					this.entries.add(item);
				}
			}
			new Handler().post(new Runnable() {
				@Override
				public void run() {
					notifyDataSetChanged();
				}
			});
		}

		new Handler().post(new Runnable() {
			@Override
			public void run() {
				notifyDataSetChanged();
			}
		});
	}

	public void addItem(DataItemClass object) {
		if(object != null) {
			this.entries.add(object);
			new Handler().post(new Runnable() {
				@Override
				public void run() {
					notifyDataSetChanged();
				}
			});
		}
	}

	public void addItem(DataItemClass object, int index) {
		if(object != null) {
			this.entries.add(index, object);
			new Handler().post(new Runnable() {
				@Override
				public void run() {
					notifyDataSetChanged();
				}
			});
		}
	}

	public void removeItem(DataItemClass object) {
		if(object != null) {
			this.entries.remove(object);
			new Handler().post(new Runnable() {
				@Override
				public void run() {
					notifyDataSetChanged();
				}
			});
		}
	}

	@Override
	public int getItemCount() {
		return entries.size() + (hasPagination && entries.size() > 0 ? 1 : 0);
	}

	public void setHasPagination(boolean hasPagination) {
		this.hasPagination = hasPagination;
		notifyDataSetChanged();
	}

	public DataItemClass getItem(int position) {
		if(hasPagination && position == getItemCount() - 1 - PAGE_THRESHOLD) {
			loadMoreEntries();
		}

		return position == entries.size() ? null : entries.get(position);
	}

	@Override
	public int getItemViewType(int position) {
		if(hasPagination && position == getItemCount() - 1) {
			return TYPE_LOADING;
		}
		else {
			return TYPE_NORMAL;
		}
	}

	public boolean contains(DataItemClass item) {
		return entries.contains(item);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void onEmptyViewPrimaryButtonPressed() {
	}

	public void onEmptyViewSecondaryButtonPressed() {
	}

	@Override
	public SimpleRVHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		switch(viewType) {
			case TYPE_NORMAL:
				ViewClass view = (ViewClass) LayoutInflater.from(parent.getContext()).inflate(rowLayoutId, parent, false);
				view.setOnClickListener(this);
				return new SimpleRVHolder(view);

			case TYPE_LOADING:
				tvLoadMoreMessage =
						(TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.tv_loadmore_message, parent, false);
				return new SimpleRVHolder(tvLoadMoreMessage);
		}

		return null;
	}

	@Override
	public void onBindViewHolder(SimpleRVHolder holder, int position) {
		switch(getItemViewType(position)) {
			case TYPE_NORMAL:
				holder.itemView.setTag(position);
				((ListItemInterface) holder.itemView).setData(getItem(position));
				break;

			case TYPE_LOADING:
				getItem(position);
				break;
		}
	}

	@Override
	public void onClick(View v) {
		onItemClicked(getItem((int) v.getTag()));
	}

	@Override
	public boolean onLongClick(View v) {
		onItemClicked(getItem((int) v.getTag()));
		return true;
	}

	/**
	 * Callback for when more entries should be retrieved from the server. Override to use
	 * Called when {@link AbsPaginatedEvTolerantRecyclerViewAdapter#getItem} is called for a position which is greater than
	 * {@link AbsPaginatedEvTolerantRecyclerViewAdapter#getItemCount()} - {@value PAGE_THRESHOLD}
	 */
	public void loadMoreEntries() {
	}

	/**
	 * Callback for when an item in the list is pressed
	 * Override to use
	 *
	 * @param item Data for the row pressed
	 */
	public void onItemClicked(DataItemClass item) {
	}

	/**
	 * Callback for when an item in the list is long pressed
	 * Override to use
	 * @param item Data for the row pressed
	 */
	public void onItemLongClicked(DataItemClass item) {
	}

}
