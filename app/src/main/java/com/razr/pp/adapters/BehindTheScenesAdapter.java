package com.razr.pp.adapters;

import com.razr.pp.R;
import com.razr.pp.models.CategoryPhotos;
import com.razr.pp.widgets.BehindTheScenesWidget;

public class BehindTheScenesAdapter extends AbsPaginatedEvTolerantRecyclerViewAdapter<CategoryPhotos, BehindTheScenesWidget>{
    public BehindTheScenesAdapter() {
        super(R.layout.row_behind_the_scenes, false);
    }

    @Override
    public void onItemClicked(CategoryPhotos item) {
        super.onItemClicked(item);
    }
}
