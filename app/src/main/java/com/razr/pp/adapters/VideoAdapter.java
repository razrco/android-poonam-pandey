package com.razr.pp.adapters;

import com.razr.pp.R;
import com.razr.pp.models.Video;
import com.razr.pp.widgets.VideosWidget;

public class VideoAdapter extends AbsPaginatedEvTolerantRecyclerViewAdapter<Video, VideosWidget>{

    public VideoAdapter() {
        super(R.layout.row_video, false);
    }

    @Override
    public void onItemClicked(Video item) {
        super.onItemClicked(item);
    }
}
