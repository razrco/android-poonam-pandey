package com.razr.pp.adapters;

import com.razr.pp.R;
import com.razr.pp.models.Album;
import com.razr.pp.widgets.AlbumWidget;

public class AlbumAdapter extends AbsPaginatedEvTolerantRecyclerViewAdapter<Album, AlbumWidget>{

    public AlbumAdapter() {
        super(R.layout.row_album, false);
    }

    @Override
    public void onItemClicked(Album item) {
        super.onItemClicked(item);
    }
}
