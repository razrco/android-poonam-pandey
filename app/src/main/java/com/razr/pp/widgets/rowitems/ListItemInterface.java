package com.razr.pp.widgets.rowitems;

public interface ListItemInterface<ItemClass> {

	void setData(ItemClass item);
}
