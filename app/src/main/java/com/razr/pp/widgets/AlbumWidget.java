package com.razr.pp.widgets;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.razr.pp.R;
import com.razr.pp.models.Album;
import com.razr.pp.retrofit.ApiClient;
import com.razr.pp.utils.ImageUtils;
import com.razr.pp.utils.Utils;
import com.razr.pp.utils.ViewUtils;
import com.razr.pp.widgets.rowitems.ListItemInterface;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlbumWidget extends CardView implements ListItemInterface<Album>{

    private Context context;

    @BindView(R.id.tvUploadDate)
    TextView tvUploadDate;

    @BindView(R.id.ivAlbumPreviewImage)
    ImageView ivAlbumPreviewImage;

    @BindView(R.id.tvPhotosCount)
    TextView tvPhotosCount;

    @BindView(R.id.tvAlbumTitle)
    TextView tvAlbumTitle;

    @BindView(R.id.tvInteractions)
    TextView tvInteractions;

    public AlbumWidget(Context context) {
        this(context, null);
        this.context = context;
    }

    public AlbumWidget(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AlbumWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        View view = LayoutInflater.from(context).inflate(R.layout.widget_album, this, false);
        ButterKnife.bind(this, view);

        addView(view);
    }

    @Override
    public void setData(Album item) {
        if(item != null){
            ViewUtils.setText(tvUploadDate, item.epochDate);
            ImageUtils.loadImage(ivAlbumPreviewImage, ApiClient.IMAGE_BASE_URL+item.image);
            ViewUtils.setText(tvPhotosCount, item.photo_count);
            ViewUtils.setText(tvAlbumTitle, item.title);
            int interactions = Integer.parseInt(item.like_count) + Integer.parseInt(item.comment_count);
            ViewUtils.setText(tvInteractions, String.valueOf(interactions)+" interactions");
        }
    }
}
