package com.razr.pp.widgets;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.razr.pp.R;


public class EmptyView extends FrameLayout {


    //private ImageView imgIcon/*, imgLoading*/;
    private ProgressBar progressBar;
    public TextView tvRetryMessage;
    public TextView tvTitle;
    public TextView tvMessage;

    public EmptyView(Context context) {
        this(context, null);
    }

    public EmptyView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EmptyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater.from(this.getContext()).inflate(R.layout.new_empty_view, this, true);
        this.tvMessage = (TextView) findViewById(R.id.tvMessage);
        this.tvTitle = (TextView) findViewById(R.id.tvTitle);
        this.tvRetryMessage = (TextView) findViewById(R.id.tvRetryMessage);
        this.progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //this.imgLoading = (ImageView) findViewById(R.id.imgLoading);
        hideEmptyView();
    }

    public void setData(@DrawableRes int iconResourceId, CharSequence retryMessage,
                        CharSequence title, CharSequence description, OnClickListener callback) {

//        switch (iconResourceId) {
//            case -1:
//                imgIcon.setVisibility(View.GONE);
//                imgLoading.setVisibility(View.GONE);
//                break;
//            default:
//                imgIcon.setVisibility(View.GONE);
//                imgLoading.setVisibility(View.VISIBLE);
//                imgLoading.setImageResource(iconResourceId);
//                Drawable drawable = imgLoading.getDrawable();
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    if (drawable instanceof AnimatedVectorDrawable) {
//                        ((AnimatedVectorDrawable) drawable).start();
//                    }
//                } else if (drawable instanceof AnimatedVectorDrawableCompat) {
//                    ((AnimatedVectorDrawableCompat) drawable).start();
//                }
//                break;
//        }
//        imgIcon.setVisibility(View.VISIBLE);
//        imgIcon.setImageResource(iconResourceId);
        progressBar.setVisibility(View.GONE);

        //imgIcon.setOnClickListener(callback);

        tvRetryMessage.setVisibility(TextUtils.isEmpty(retryMessage) ? View.GONE : View.VISIBLE);
        tvRetryMessage.setText(retryMessage);

        tvTitle.setVisibility(TextUtils.isEmpty(title) ? View.GONE : View.VISIBLE);
        tvTitle.setText(title);

        tvMessage.setVisibility(TextUtils.isEmpty(description) ? View.GONE : View.VISIBLE);
        tvMessage.setText(description);

        if (getVisibility() != VISIBLE) {
            setVisibility(VISIBLE);
        }
    }

    public void hideEmptyView() {
        setVisibility(GONE);
    }

    public void displayLoadingMessage() {
        //setData(R.drawable.logo_quirk, null, null, null, null);
        //imgIcon.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);


        tvRetryMessage.setVisibility(GONE);

        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText("Loading");

        tvMessage.setVisibility(View.VISIBLE);
        tvMessage.setText("Please wait while we load the contents");

        if (getVisibility() != VISIBLE) {
            setVisibility(VISIBLE);
        }
    }
}
