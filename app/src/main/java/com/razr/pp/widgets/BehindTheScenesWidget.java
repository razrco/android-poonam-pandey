package com.razr.pp.widgets;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.razr.pp.R;
import com.razr.pp.models.CategoryPhotos;
import com.razr.pp.retrofit.ApiClient;
import com.razr.pp.utils.ImageUtils;
import com.razr.pp.utils.ViewUtils;
import com.razr.pp.widgets.rowitems.ListItemInterface;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BehindTheScenesWidget extends CardView implements ListItemInterface<CategoryPhotos>{

    @BindView(R.id.tvUploadDate)
    TextView tvUploadDate;

    @BindView(R.id.ivBehindTheScenesImage)
    ImageView ivBehindTheScenesImage;

    @BindView(R.id.tvBehindTheScenesTitle)
    TextView tvBehindTheScenesTitle;

    @BindView(R.id.ivLike)
    ImageView ivLike;

    @BindView(R.id.ivComment)
    ImageView ivComment;

    @BindView(R.id.ivShare)
    ImageView ivShare;

    @BindView(R.id.ivDownload)
    ImageView ivDownload;

    @BindView(R.id.tvInteractions)
    TextView tvInteractions;

    private Context context;
    public BehindTheScenesWidget(Context context) {
        this(context, null);
        this.context = context;
    }

    public BehindTheScenesWidget(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BehindTheScenesWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        View view = LayoutInflater.from(context).inflate(R.layout.widget_behind_the_scenes, this, false);
        ButterKnife.bind(this, view);

        addView(view);
    }

    @Override
    public void setData(CategoryPhotos item) {
        if(item != null){
            ViewUtils.setText(tvUploadDate, item.epochDate);
            ImageUtils.loadImage(ivBehindTheScenesImage, ApiClient.IMAGE_BASE_URL+item.image);
            ViewUtils.setText(tvBehindTheScenesTitle, item.title);
            int interactions = Integer.parseInt(item.like_count) + Integer.parseInt(item.comment_count);
            ViewUtils.setText(tvInteractions, String.valueOf(interactions)+" interactions");
        }
    }
}
