package com.razr.pp.widgets;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.razr.pp.R;
import com.razr.pp.models.Feed;
import com.razr.pp.utils.ImageUtils;
import com.razr.pp.utils.ViewUtils;
import com.razr.pp.widgets.rowitems.ListItemInterface;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SocialMediaWidget extends CardView implements ListItemInterface<Feed>{

    private Context context;
    @BindView(R.id.tvDate)
    TextView tvDate;

    @BindView(R.id.ivSocialIcon)
    ImageView ivSocialIcon;

    @BindView(R.id.tvContentDescription)
    TextView tvContentDescription;

    @BindView(R.id.ivContentImage)
    ImageView ivContentImage;

    @BindView(R.id.tvLike)
    TextView tvLike;

    @BindView(R.id.tvComment)
    TextView tvComment;

    @BindView(R.id.ivShare)
    ImageView ivShare;


    public SocialMediaWidget(Context context) {
        this(context, null);
        this.context = context;
    }

    public SocialMediaWidget(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SocialMediaWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        View view =  LayoutInflater.from(context).inflate(R.layout.widget_social_media, this, false);
        ButterKnife.bind(this, view);

        addView(view);
    }

    @Override
    public void setData(Feed item) {
        if(item != null) {
            ViewUtils.setText(tvDate, item.created_date);
            ViewUtils.setText(tvContentDescription, item.title);
            ImageUtils.loadImage(ivContentImage, item.picture);
            ivContentImage.setVisibility(VISIBLE);
            ViewUtils.setText(tvLike, item.like_count);
            ViewUtils.setText(tvComment, item.comment_count);
        }
    }
}
