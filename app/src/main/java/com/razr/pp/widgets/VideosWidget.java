package com.razr.pp.widgets;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.razr.pp.R;
import com.razr.pp.models.Video;
import com.razr.pp.retrofit.ApiClient;
import com.razr.pp.utils.ImageUtils;
import com.razr.pp.utils.ViewUtils;
import com.razr.pp.widgets.rowitems.ListItemInterface;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideosWidget extends CardView implements ListItemInterface<Video>{

    @BindView(R.id.ivVideoPreview)
    ImageView ivVideoPreview;

    @BindView(R.id.ivCircularImage)
    ImageView ivCircularImage;

    @BindView(R.id.tvVideoTitle)
    TextView tvVideoTitle;

    @BindView(R.id.tvInteractions)
    TextView tvInteractions;

    @BindView(R.id.tvUploadDate)
    TextView tvUploadDate;

    private Context context;
    public VideosWidget(Context context) {
        this(context, null);
        this.context = context;
    }

    public VideosWidget(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VideosWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        View view = LayoutInflater.from(context).inflate(R.layout.widget_video, this, false);
        ButterKnife.bind(this, view);

        addView(view);
    }

    @Override
    public void setData(Video item) {
        if(item != null){
            ImageUtils.loadImage(ivVideoPreview, ApiClient.IMAGE_BASE_URL+item.image);
            ImageUtils.loadImage(ivCircularImage, ApiClient.IMAGE_BASE_URL+item.image);
            ViewUtils.setText(tvVideoTitle, item.title);
            int interactions = Integer.parseInt(item.like_count) + Integer.parseInt(item.comment_count);
            ViewUtils.setText(tvInteractions, String.valueOf(interactions)+" interactions");
            ViewUtils.setText(tvUploadDate, item.epochDate);
        }
    }
}
