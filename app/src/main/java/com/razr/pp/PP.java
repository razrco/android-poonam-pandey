package com.razr.pp;

import android.app.Application;
import android.content.Context;

public class PP extends Application {

    private static volatile Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    public static Context getPPAppContext() {
        return PP.context;
    }
}
