package com.razr.pp.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Location implements Parcelable {
    public String epochDate;
    public String created_date;
    public String id;
    public String title;
    public String slug;
    public String like_count;
    public String comment_count;
    public ArrayList<Place> places;
    public ArrayList<Video> videos;
    public String image;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.epochDate);
        dest.writeString(this.created_date);
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.slug);
        dest.writeString(this.like_count);
        dest.writeString(this.comment_count);
        dest.writeList(this.places);
        dest.writeList(this.videos);
        dest.writeString(this.image);
    }

    public Location() {
    }

    protected Location(Parcel in) {
        this.epochDate = in.readString();
        this.created_date = in.readString();
        this.id = in.readString();
        this.title = in.readString();
        this.slug = in.readString();
        this.like_count = in.readString();
        this.comment_count = in.readString();
        this.places = new ArrayList<Place>();
        in.readList(this.places, Place.class.getClassLoader());
        this.videos = new ArrayList<Video>();
        in.readList(this.videos, Video.class.getClassLoader());
        this.image = in.readString();
    }

    public static final Parcelable.Creator<Location> CREATOR = new Parcelable.Creator<Location>() {
        @Override
        public Location createFromParcel(Parcel source) {
            return new Location(source);
        }

        @Override
        public Location[] newArray(int size) {
            return new Location[size];
        }
    };
}
