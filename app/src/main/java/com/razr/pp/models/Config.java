package com.razr.pp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Config implements Parcelable {
    public String id;
    public String title;
    public String celeb_id;
    public String storage_url;
    public boolean published;
    public String created_at;
    public String updated_at;
    public String created_by;
    public String update_by;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.celeb_id);
        dest.writeString(this.storage_url);
        dest.writeByte(this.published ? (byte) 1 : (byte) 0);
        dest.writeString(this.created_at);
        dest.writeString(this.updated_at);
        dest.writeString(this.created_by);
        dest.writeString(this.update_by);
    }

    public Config() {
    }

    protected Config(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.celeb_id = in.readString();
        this.storage_url = in.readString();
        this.published = in.readByte() != 0;
        this.created_at = in.readString();
        this.updated_at = in.readString();
        this.created_by = in.readString();
        this.update_by = in.readString();
    }

    public static final Parcelable.Creator<Config> CREATOR = new Parcelable.Creator<Config>() {
        @Override
        public Config createFromParcel(Parcel source) {
            return new Config(source);
        }

        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };
}
