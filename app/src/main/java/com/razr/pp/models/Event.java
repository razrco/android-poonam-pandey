package com.razr.pp.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Event implements Parcelable {
    public String epochDate;
    public String created_date;
    public String id;
    public String title;
    public String slug;
    public String photo_count;
    public String like_count;
    public String comment_count;
    public ArrayList<Photo> photos;
    public String image;
    public ArrayList<Video> videos;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.epochDate);
        dest.writeString(this.created_date);
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.slug);
        dest.writeString(this.photo_count);
        dest.writeString(this.like_count);
        dest.writeString(this.comment_count);
        dest.writeTypedList(this.photos);
        dest.writeString(this.image);
        dest.writeList(this.videos);
    }

    public Event() {
    }

    protected Event(Parcel in) {
        this.epochDate = in.readString();
        this.created_date = in.readString();
        this.id = in.readString();
        this.title = in.readString();
        this.slug = in.readString();
        this.photo_count = in.readString();
        this.like_count = in.readString();
        this.comment_count = in.readString();
        this.photos = in.createTypedArrayList(Photo.CREATOR);
        this.image = in.readString();
        this.videos = new ArrayList<Video>();
        in.readList(this.videos, Video.class.getClassLoader());
    }

    public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel source) {
            return new Event(source);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
}
