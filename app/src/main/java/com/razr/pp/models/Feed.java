package com.razr.pp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Feed implements Parcelable {
    public String epochDate;
    public String created_date;
    public String id;
    public String title;
    public String body;
    public String picture;
    public String v_id;
    public String type;
    public boolean published;
    public String like_count;
    public String comment_count;
    public String media_count;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.epochDate);
        dest.writeString(this.created_date);
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.body);
        dest.writeString(this.picture);
        dest.writeString(this.v_id);
        dest.writeString(this.type);
        dest.writeByte(this.published ? (byte) 1 : (byte) 0);
        dest.writeString(this.like_count);
        dest.writeString(this.comment_count);
        dest.writeString(this.media_count);
    }

    public Feed() {
    }

    protected Feed(Parcel in) {
        this.epochDate = in.readString();
        this.created_date = in.readString();
        this.id = in.readString();
        this.title = in.readString();
        this.body = in.readString();
        this.picture = in.readString();
        this.v_id = in.readString();
        this.type = in.readString();
        this.published = in.readByte() != 0;
        this.like_count = in.readString();
        this.comment_count = in.readString();
        this.media_count = in.readString();
    }

    public static final Parcelable.Creator<Feed> CREATOR = new Parcelable.Creator<Feed>() {
        @Override
        public Feed createFromParcel(Parcel source) {
            return new Feed(source);
        }

        @Override
        public Feed[] newArray(int size) {
            return new Feed[size];
        }
    };
}
