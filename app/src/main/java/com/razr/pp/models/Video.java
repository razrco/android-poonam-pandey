package com.razr.pp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Video implements Parcelable {
    public String epochDate;
    public String created_date;
    public String id;
    public String title;
    public String slug;
    public String description;
    public Category category;
    public String player_type;
    public String like_count;
    public String comment_count;
    public String embed;
    public Genre genre;
    public Language language;
    public String image;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.epochDate);
        dest.writeString(this.created_date);
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.slug);
        dest.writeString(this.description);
        dest.writeParcelable(this.category, flags);
        dest.writeString(this.player_type);
        dest.writeString(this.like_count);
        dest.writeString(this.comment_count);
        dest.writeString(this.embed);
        dest.writeParcelable(this.genre, flags);
        dest.writeParcelable(this.language, flags);
        dest.writeString(this.image);
    }

    public Video() {
    }

    protected Video(Parcel in) {
        this.epochDate = in.readString();
        this.created_date = in.readString();
        this.id = in.readString();
        this.title = in.readString();
        this.slug = in.readString();
        this.description = in.readString();
        this.category = in.readParcelable(Category.class.getClassLoader());
        this.player_type = in.readString();
        this.like_count = in.readString();
        this.comment_count = in.readString();
        this.embed = in.readString();
        this.genre = in.readParcelable(Genre.class.getClassLoader());
        this.language = in.readParcelable(Language.class.getClassLoader());
        this.image = in.readString();
    }

    public static final Parcelable.Creator<Video> CREATOR = new Parcelable.Creator<Video>() {
        @Override
        public Video createFromParcel(Parcel source) {
            return new Video(source);
        }

        @Override
        public Video[] newArray(int size) {
            return new Video[size];
        }
    };
}