package com.razr.pp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Genre implements Parcelable {
    public String id;
    public String title;
    public String description;
    public boolean published;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeByte(this.published ? (byte) 1 : (byte) 0);
    }

    public Genre() {
    }

    protected Genre(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.published = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Genre> CREATOR = new Parcelable.Creator<Genre>() {
        @Override
        public Genre createFromParcel(Parcel source) {
            return new Genre(source);
        }

        @Override
        public Genre[] newArray(int size) {
            return new Genre[size];
        }
    };
}
