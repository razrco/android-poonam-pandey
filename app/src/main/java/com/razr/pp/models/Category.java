package com.razr.pp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Category implements Parcelable {
    public String id;
    public String title;
    public String slug;
    public boolean published;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.slug);
        dest.writeByte(this.published ? (byte) 1 : (byte) 0);
    }

    public Category() {
    }

    protected Category(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.slug = in.readString();
        this.published = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
