package com.razr.pp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Photo implements Parcelable {
    public String epochDate;
    public String created_date;
    public String id;
    public String title;
    public String slug;
    public String like_count;
    public String comment_count;
    public String image;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.epochDate);
        dest.writeString(this.created_date);
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.slug);
        dest.writeString(this.like_count);
        dest.writeString(this.comment_count);
        dest.writeString(this.image);
    }

    public Photo() {
    }

    protected Photo(Parcel in) {
        this.epochDate = in.readString();
        this.created_date = in.readString();
        this.id = in.readString();
        this.title = in.readString();
        this.slug = in.readString();
        this.like_count = in.readString();
        this.comment_count = in.readString();
        this.image = in.readString();
    }

    public static final Parcelable.Creator<Photo> CREATOR = new Parcelable.Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };
}
